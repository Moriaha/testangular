import { Component, OnInit , Output} from '@angular/core';

@Component({
  selector: 'jce-hard-coded',
  templateUrl: './hard-coded.component.html',
  styleUrls: ['./hard-coded.component.css'],
  inputs:["hard"]
})
export class HardCodedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}