import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jce-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css'],
  inputs:['visible']
})


export class SpinnerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}