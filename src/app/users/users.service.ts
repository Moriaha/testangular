import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class UsersService {

//api connection:

private _url ='http://moriaha.myweb.jce.ac.il/api/users';

getUsrsFromApi(){
	  return this._http.get(this._url).map(res => res.json());
}


//JSON place holder connection:

/*private _url ='http://jsonplaceholder.typicode.com/users';

getUsers(){
  return this._http.get(this._url).map(res => res.json()).delay(200);
}*/


  constructor(private _http:Http) { }

}