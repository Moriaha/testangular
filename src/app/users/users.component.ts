import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users li {cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active,
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
  `]
})
export class UsersComponent implements OnInit {

users;
currentUser;
isLoading = true;

select(user){
  this.currentUser = user;
}

deleteUser(user){
this.users.splice(this.users.indexOf(user),1)
}

editUser(user){
  this.users[this.users.indexOf(user)].name=user.name;
  this.users[this.users.indexOf(user)].email=user.email;
}

addUser(user){
  this.users.push(user);
}
  constructor(private _userService:UsersService) { }

  ngOnInit() {
    //JSON place holder connection
    //this._userService.getUsers().subscribe(userData => {this.users=userData ; this.isLoading = false});
    
    //api connection:
    this._userService.getUsrsFromApi().subscribe(usersData =>{this.users=usersData.result; this.isLoading=false});
  }

}