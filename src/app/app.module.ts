import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import{RouterModule, Routes} from '@angular/router';
import{AngularFireModule} from 'angularfire2';


import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { PostsComponent } from './posts/posts.component';
import { PostComponent } from './post/post.component';
import { UserComponent } from './user/user.component';
import { PostsService } from './posts/posts.service';
import { HardCodedComponent } from './hard-coded/hard-coded.component';
import { HardCodedsComponent } from './hard-codeds/hard-codeds.component';
import { HardCodedsService } from './hard-codeds/hard-codeds.service';
import { SpinnerComponent } from './Shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryComponent } from './category/category.component';
import { ProductsService } from './products/products.service';
import { CategoriesService } from './categories/categories.service';
import { InvoicesComponentComponent } from './invoices-component/invoices-component.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesService } from './invoices-component/invoices.service';
import { InvoiceComponent } from './invoice/invoice.component';




    export const firebaseConfig = {
    apiKey: "AIzaSyBNpHJbD4-sllxZhAQNvSk5s5e3n-HcRjk",
    authDomain: "angular-4d52c.firebaseapp.com",
    databaseURL: "https://angular-4d52c.firebaseio.com",
    storageBucket: "angular-4d52c.appspot.com",
    messagingSenderId: "480878009474"
  };

		const appRoutes: Routes =[
      {path:'invoiceForm',component:InvoiceFormComponent},
      {path:'invoicesComponent',component:InvoicesComponentComponent},
		  {path:'',component:InvoiceFormComponent},
		  {path:'**',component:PageNotFoundComponent}
		]; 

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent,
    PostComponent,
    UserComponent,
    HardCodedComponent,
    HardCodedsComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent,
    ProductsComponent,
    ProductComponent,
    CategoriesComponent,
    CategoryComponent,
    CategoriesComponent,
    InvoicesComponentComponent,
    InvoiceFormComponent,
    InvoiceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService, PostsService,HardCodedsService,ProductsService,CategoriesService,InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }