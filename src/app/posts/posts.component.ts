import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';


@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styles: [`
    .posts li {cursor: default; }
    .posts li:hover { background: #ecf0f1; }
    .list-group-item.active,
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
  `]
})
export class PostsComponent implements OnInit {

posts;
//posts2;
currentPost;
isLoading = true;

select(post){
this.currentPost=post;
}
addPost(post){
this._postsService.addPost(post);
}
deletePost(post){
this._postsService.deletePost(post);
}
editPost(post){
this._postsService.editPost(post);
}

  constructor(private _postsService:PostsService, private _postsService2:PostsService) { }

  ngOnInit() {
  //for connection via firebase
  this._postsService.getPosts().subscribe(postsData => {this.posts=postsData; this.isLoading=false});  
}
}