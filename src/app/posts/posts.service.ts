import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import {Http} from '@angular/http';


@Injectable()
export class PostsService {

postsObservable;


//get users from firebase without join
/*getPosts(){
this.postsObservable=this.af.database.list('/posts') ;
return this.postsObservable;
}*/

//get users from firebase with join to users
getPosts(){
this.postsObservable=this.af.database.list('/posts').map(
posts => 
  {posts.map( post=> {
    post.userName=[];
    for(var p in post.users){
      post.userName.push(this.af.database.object('/users/' + p))
    }});
    return posts;
  }
);
return this.postsObservable;
}

addPost(post){
  this.postsObservable.push(post);
}
deletePost(post){
let postKey=post.$key;
this.af.database.object('/posts/'+postKey).remove();
}
editPost(post){
  let postKey=post.$key;
  let userData ={title:post.title, content:post.content};
  this.af.database.object('/posts/'+postKey).update(userData);
}

  constructor(private af:AngularFire) { }

}