import { Component, OnInit } from '@angular/core';
import {Category} from './category';

@Component({
  selector: 'jce-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
