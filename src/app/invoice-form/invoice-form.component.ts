import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import {NgForm} from '@angular/forms';
import{Invoice} from '../invoices-component/invoice';
import { InvoicesService } from '../invoices-component/invoices.service';

@Component({
  selector: 'jce-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {


invoice:Invoice={name:'',amount:null};
invoices;
fillName = true;
fillAmount = true;


onSubmit(form:NgForm){

//validation
    if (  this.invoice.name == ''){
      this.fillName=false;
    }
    else{
      this.fillName=true;
    }
    if (this.invoice.amount == null){
      this.invoice.amount=1000;
    }
    else{
      this.fillAmount=true;
    }
  if(this.invoice.name != '' && this.invoice.amount != null){
    this._invoicesService.addInvoice(this.invoice);
    console.log(this.invoice);
    this.invoice={name:'',amount:null};}
}

  constructor(private _invoicesService:InvoicesService) { }

  ngOnInit() {
    this._invoicesService.getPosts().subscribe(postsData => {this.invoices=postsData});
  }

}
