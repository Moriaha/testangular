import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class InvoicesService {

invoiceObservable;


getPosts(){
this.invoiceObservable=this.af.database.list('/invoices').delay(500) ;
return this.invoiceObservable;
}

addInvoice(invoice){
   this.invoiceObservable.push(invoice);
}

  constructor(private af:AngularFire) { }

}
