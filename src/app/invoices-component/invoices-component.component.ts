import { Component, OnInit, Output, EventEmitter , Input} from '@angular/core';
import{Invoice} from './invoice'
import { InvoicesService } from './invoices.service';
import { InvoiceFormComponent } from '../invoice-form/invoice-form.component';


@Component({
  selector: 'jce-invoices-component',
  templateUrl: './invoices-component.component.html', 
 // template: '<jce-invoices-component #jce-invoices-component></jce-invoices-component><jce-invoice-form (invoiceAddedEvent)="addInvoice($event)"> </jce-invoice-form>',
    styles: [`
    .invoices li {cursor: default; }
    .invoices li:hover { background: #ecf0f1; }
    .list-group-item.active,
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
  `]
})
export class InvoicesComponentComponent implements OnInit {

invoice:Invoice;
invoices;
isLoading=true;
currentInvoice;

select(invoice){
this.currentInvoice=invoice;
}
addInvoice(invoice){
this._invoicesService.addInvoice(invoice);

}

  constructor(private _invoicesService:InvoicesService) { }

  ngOnInit() {
    this._invoicesService.getPosts().subscribe(postsData => {this.invoices=postsData; this.isLoading=false});  
}
  

}
