import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post'

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:["post"]
})
export class PostComponent implements OnInit {
@Output() deleteEvent = new EventEmitter<Post>();
@Output() editEvent = new EventEmitter<Post>();

post:Post;
isEdit=false;
title1;
content1;
editButtonText='Edit';

toggleEdit(){
  this.isEdit=!this.isEdit
  this.isEdit ? this.editButtonText='Save':this.editButtonText='Edit';
  this.isEdit ? (this.title1=this.post.title , this.content1=this.post.content):this.editEvent.emit(this.post);
}

cancelEdit(){
  this.post.title=this.title1;
  this.post.content=this.content1;
  this.isEdit=!this.isEdit;
  this.editButtonText='Edit';
}
sendDelete(){
this.deleteEvent.emit(this.post);

}
  constructor() { }

  ngOnInit() {
  }

}