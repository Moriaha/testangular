import { Component, OnInit , Output, EventEmitter } from '@angular/core';
import{User} from './user'

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<User>();
  @Output() editEvent = new EventEmitter<User>();

  user:User;
  isEdit=false;
  editButtonText='Edit';
  name1;
  email1;

  sendDelete(){
    this.deleteEvent.emit(this.user);
  }

  toggleEdit(){
    this.isEdit=!this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
    this.isEdit ? (this.name1=this.user.name , this.email1=this.user.email) : this.editEvent.emit(this.user);
  }

  cancelEdit(){
    this.user.name=this.name1;
    this.user.email=this.email1;
    this.isEdit=!this.isEdit;
    this.editButtonText='Edit';
  }

  constructor() { }

  ngOnInit() {
  }
}