/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { HardCodedsComponent } from './hard-codeds.component';

describe('HardCodedsComponent', () => {
  let component: HardCodedsComponent;
  let fixture: ComponentFixture<HardCodedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HardCodedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HardCodedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
