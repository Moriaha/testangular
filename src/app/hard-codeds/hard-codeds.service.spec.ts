/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { HardCodedsService } from './hard-codeds.service';

describe('Service: HardCodeds', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HardCodedsService]
    });
  });

  it('should ...', inject([HardCodedsService], (service: HardCodedsService) => {
    expect(service).toBeTruthy();
  }));
});
