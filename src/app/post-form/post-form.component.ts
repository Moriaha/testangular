import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Post} from '../post/post';

@Component({
  selector: 'jce-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
@Output() postAddedEvent = new EventEmitter<Post>();

post:Post = {title:'',content:'' , user:{name:'',email:''}};


  constructor() { }
onSubmit(form:NgForm){
this.postAddedEvent.emit(this.post);
//this.post= {title:'',content:''};
this.post = {title:'',content:'' , user:{name:'',email:''}};
}
  ngOnInit() {
  }

}